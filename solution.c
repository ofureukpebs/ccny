// Name: Ofure Ukpebor 
// Fundamental Algorithms Homework 2

/* To compile: gcc -Wall solution.c -o solution.out -I/usr/X11R6/include -L/usr/X11R6/lib -I/opt/X11/include -lX11 */
/* To run: ./solution.out*/

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xos.h>
#include <X11/Xatom.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <limits.h>

Display *display_ptr;
Screen *screen_ptr;
int screen_num;
char *display_name = NULL;
unsigned int display_width, display_height;

Window win;
int border_width;
unsigned int win_width, win_height;
int win_x, win_y;

XWMHints *wm_hints;
XClassHint *class_hints;
XSizeHints *size_hints;
XTextProperty win_name, icon_name;
char *win_name_string = "Ofure Ukpebor Hw2";
char *icon_name_string = "Icon for Example Window";

XEvent report;

GC gc, gc_yellow, gc_red, gc_blue, gc_black, gc_white;
GC gc, gc_yellow_thick_line, gc_red_thick_line, gc_blue_thick_line;

unsigned long valuemask = 0;
XGCValues gc_values, gc_yellow_values, gc_red_values, gc_blue_values, gc_white_values;
XGCValues gc_yellow_thick_values, gc_red_thick_values, gc_blue_thick_values;

Colormap color_map;
XColor tmp_color1, tmp_color2;

typedef int bool;
  #define true 1
  #define false 0

typedef struct InputPoint{
  int x;
  int y;
}InputPoint;

typedef struct Server{
  int x;
  int y;
}Server;

int boundingX = 500;
int boundingY = 500;

//#define LIM SHRT_MAX //causes kill:9 error
#define LIM       65535
#define LIM_REQUEST_COUNT 60
#define SERVER_SIZE  30 //30 is height of server rectangle
#define YELLOW_SERVER_POS_X 0
#define YELLOW_SERVER_POS_Y 0
#define RED_SERVER_POS_X boundingX - SERVER_SIZE
#define RED_SERVER_POS_Y 0
#define BLUE_SERVER_POS_X boundingX/2
#define BLUE_SERVER_POS_Y boundingX-30


int proximityServerYellow, proximityServerRed, proximityServerBlue;
int maxMatrixArr [LIM_REQUEST_COUNT][LIM_REQUEST_COUNT][LIM_REQUEST_COUNT][LIM_REQUEST_COUNT];
int costMatrixArr [LIM_REQUEST_COUNT][LIM_REQUEST_COUNT][LIM_REQUEST_COUNT][LIM_REQUEST_COUNT];
int onlineStrategyCost = 0;
int offlineStrategyCost = 0;
bool acceptingRequests = true;
int requestCount;


InputPoint serviceRequests[LIM_REQUEST_COUNT];
Server yellowServer;
Server redServer;
Server blueServer;
Server serverSent; //server attending to the request

void positionServers();
void sendServerToPoint(int x, int y);
int calculateProximity(struct InputPoint request, struct Server server);
void calculateOfflineStrategy(int x, int y);
int calculateCost(InputPoint req1, InputPoint req2);
int cheapestCost(int reqI, int reqJ, int reqK);
void printReport();
void resetRequestQueues();


int main(int argc, char **argv)
{  
  /* opening display: basic connection to X Server */
  if( (display_ptr = XOpenDisplay(display_name)) == NULL )
    { printf("Could not open display. \n"); exit(-1);}
  
  screen_num = DefaultScreen( display_ptr );
  screen_ptr = DefaultScreenOfDisplay( display_ptr );
  color_map  = XDefaultColormap( display_ptr, screen_num );
  display_width  = DisplayWidth( display_ptr, screen_num );
  display_height = DisplayHeight( display_ptr, screen_num );
  /* creating the window */
  border_width = 10;
  win_x = 0; win_y = 0;

 /*bound window*/
  win_width  = boundingX;
  win_height = boundingY;

  win= XCreateSimpleWindow( display_ptr, RootWindow( display_ptr, screen_num), win_x, win_y, win_width, win_height, border_width, BlackPixel(display_ptr, screen_num), WhitePixel(display_ptr, screen_num) );
                            
  /* now try to put it on screen, this needs cooperation of window manager */
  size_hints = XAllocSizeHints();
  wm_hints = XAllocWMHints();
  class_hints = XAllocClassHint();
  if( size_hints == NULL || wm_hints == NULL || class_hints == NULL )
    { printf("Error allocating memory for hints. \n"); exit(-1);}

  size_hints -> flags = PPosition | PSize | PMinSize ;
  size_hints -> min_width = 60;
  size_hints -> min_height = 60;

  XStringListToTextProperty( &win_name_string,1,&win_name);
  XStringListToTextProperty( &icon_name_string,1,&icon_name);
  
  wm_hints -> flags = StateHint | InputHint ;
  wm_hints -> initial_state = NormalState;
  wm_hints -> input = False;

  class_hints -> res_name = "x_use_example";
  class_hints -> res_class = "examples";

  XSetWMProperties( display_ptr, win, &win_name, &icon_name, argv, argc,size_hints, wm_hints, class_hints );

  /* what events do we want to receive */
  XSelectInput( display_ptr, win, ExposureMask | StructureNotifyMask | ButtonPressMask );
  
  /* finally: put window on screen */
  XMapWindow( display_ptr, win );
  
  XFlush(display_ptr);

    gc = XCreateGC( display_ptr, win, valuemask, &gc_values);
  XSetForeground( display_ptr, gc, BlackPixel( display_ptr, screen_num ) );
  XSetLineAttributes( display_ptr, gc, 2, LineSolid, CapRound, JoinRound);
  
  /* and three other graphics contexts, to draw in yellow and red and blue*/
  gc_yellow = XCreateGC( display_ptr, win, valuemask, &gc_yellow_values);
  XSetLineAttributes(display_ptr, gc_yellow, 2, LineSolid,CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "yellow",
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color yellow\n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_yellow, tmp_color1.pixel );

  gc_yellow_thick_line = XCreateGC( display_ptr, win, valuemask, &gc_yellow_thick_values);
  XSetLineAttributes( display_ptr, gc_yellow_thick_line, 4, LineSolid, CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "yellow", 
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color yellow thick \n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_yellow_thick_line, tmp_color1.pixel );

    
  gc_red = XCreateGC( display_ptr, win, valuemask, &gc_red_values);
  XSetLineAttributes( display_ptr, gc_red, 2, LineSolid, CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "red",
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color red\n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_red, tmp_color1.pixel );
    
  gc_red_thick_line = XCreateGC( display_ptr, win, valuemask, &gc_red_thick_values);
  XSetLineAttributes( display_ptr, gc_red_thick_line, 4, LineSolid, CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "red", 
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color red thick \n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_red_thick_line, tmp_color1.pixel );

    
  gc_blue = XCreateGC( display_ptr, win, valuemask, &gc_blue_values);
  XSetLineAttributes( display_ptr, gc_blue, 2, LineSolid, CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "blue",
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color blue\n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_blue, tmp_color1.pixel );

  gc_blue_thick_line = XCreateGC( display_ptr, win, valuemask, &gc_blue_thick_values);
  XSetLineAttributes( display_ptr, gc_blue_thick_line, 4, LineSolid, CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "blue", 
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color blue thick \n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_blue_thick_line, tmp_color1.pixel );  

    gc_white = XCreateGC( display_ptr, win, valuemask, &gc_white_values);
  XSetLineAttributes( display_ptr, gc_white, 4, LineSolid, CapRound, JoinRound);
  if( XAllocNamedColor( display_ptr, color_map, "white", 
      &tmp_color1, &tmp_color2 ) == 0 )
    {printf("failed to get color white\n"); exit(-1);} 
  else
    XSetForeground( display_ptr, gc_white, tmp_color1.pixel );  
   
  while(1){
      XNextEvent( display_ptr, &report );
        switch( report.type )
        {
            case Expose:
                positionServers();      
                break;

            case ConfigureNotify:
                /* This event happens when the user changes the size of the window*/
                win_width = report.xconfigure.width;
                win_height = report.xconfigure.height;
                break;
            case ButtonPress:
            {
              if (acceptingRequests){ //still accepting requests
                  int x, y;
                  
                  x = report.xbutton.x;
                  y = report.xbutton.y;

                  if (report.xbutton.button == Button1 ) {
                    // left click
                    sendServerToPoint(x, y);   
                  }    
                  else{
                    //right click
                    if (onlineStrategyCost == 0){
                      printf("No requests points entered.\n\n");
                    }else{
                    printf("--=== Calculating strategies ===--\n");
                     calculateOfflineStrategy(x,y);
                   }
                  }
                }
                else{
                  printf("\nResetting request queues. \n");
                  resetRequestQueues();
                  //exit(0);
                }
            }
        }
    }  
    exit(0);
}

void positionServers(){
  //Top Left is Yellow Server
  XFillRectangle( display_ptr, win, gc_yellow, YELLOW_SERVER_POS_X, YELLOW_SERVER_POS_Y, SERVER_SIZE, SERVER_SIZE);
  yellowServer.x = YELLOW_SERVER_POS_X;
  yellowServer.y = YELLOW_SERVER_POS_Y;
  requestCount ++;

  //Top Right is Red Server
  XFillRectangle( display_ptr, win, gc_red, RED_SERVER_POS_X, RED_SERVER_POS_Y, SERVER_SIZE, SERVER_SIZE);
  redServer.x = RED_SERVER_POS_X;
  redServer.y = RED_SERVER_POS_Y;
  requestCount ++;


  //Bottom Middle is Blue Server
  XFillRectangle( display_ptr, win, gc_blue, BLUE_SERVER_POS_X, BLUE_SERVER_POS_Y, SERVER_SIZE, SERVER_SIZE);
  blueServer.x = BLUE_SERVER_POS_X;
  blueServer.y = BLUE_SERVER_POS_Y; 
  requestCount ++;

}

void sendServerToPoint(int x, int y){
   //x,y are point of service
  if (requestCount < LIM_REQUEST_COUNT-1){
     XFillArc(display_ptr, win, gc, x, y, 6, 6, 0, 360*64);

     serviceRequests[requestCount].x = x;
     serviceRequests[requestCount].y = y;

     //find closest server to serverRequest point
    proximityServerYellow = calculateProximity(serviceRequests[requestCount] , yellowServer);
    proximityServerRed    = calculateProximity(serviceRequests[requestCount] , redServer);
    proximityServerBlue   = calculateProximity(serviceRequests[requestCount] , blueServer);


     if (proximityServerYellow <= proximityServerBlue &&  proximityServerYellow <= proximityServerRed ){
      //Send server
      serverSent = yellowServer;

      XDrawLine(display_ptr, win, gc_yellow, serverSent.x, serverSent.y, serviceRequests[requestCount].x, serviceRequests[requestCount].y );
      //increment strategy cost
      onlineStrategyCost += proximityServerYellow;
      
      //Update server location
      yellowServer.x = serviceRequests[requestCount].x;
      yellowServer.y = serviceRequests[requestCount].y;
     }
      else if(proximityServerRed <= proximityServerBlue && proximityServerRed <= proximityServerYellow){
          //Send server
      serverSent = redServer;
      
      XDrawLine(display_ptr, win, gc_red, serverSent.x, serverSent.y, serviceRequests[requestCount].x, serviceRequests[requestCount].y );
      
      //increment strategy cost
         onlineStrategyCost += proximityServerRed;

          //Send and Update server location
          redServer.x = serviceRequests[requestCount].x;
          redServer.y = serviceRequests[requestCount].y;
     }

     else if(proximityServerBlue <= proximityServerYellow && proximityServerBlue <= proximityServerRed){
      //Send server
      serverSent = blueServer;

      XDrawLine(display_ptr, win, gc_blue, serverSent.x, serverSent.y, serviceRequests[requestCount].x, serviceRequests[requestCount].y );
       
         //increment strategy cost
        onlineStrategyCost += proximityServerBlue;

          //Send and Update server location
          blueServer.x = serviceRequests[requestCount].x;
          blueServer.y = serviceRequests[requestCount].y;
     }

     else{
       //No server to handle request
         printf("Error: Request limit reached.\nNo server can handle request that request\n");
     }
      //Send server and Update server location
      //serverSent.x = serviceRequests[requestCount].x;
      //serverSent.y = serviceRequests[requestCount].y;
      //printf("Request count %d\n", requestCount);
      //increase request counts
      requestCount++;
  }
  else{
    printf("Maximum request count is %d\n",LIM_REQUEST_COUNT);
    exit(0);
  }

   // printf("proximityServerYellow %d \n",proximityServerYellow);
   // printf("proximityServerBlue %d \n",proximityServerBlue);
   // printf("proximityServerRed %d \n",proximityServerRed);
   // printf("==============\n");
}

int calculateProximity(InputPoint request, Server server){
     /*TODO: Understand how this code works */
   int distanceToPoint = 
   (request.x - server.x) * (request.x - server.x) + (request.y - server.y) * (request.y - server.y);
   return sqrt(distanceToPoint);
}

int calculateCost(InputPoint req1, InputPoint req2){
  int cost = 
   (req1.x - req2.x) * (req1.x - req2.x) + (req1.y - req2.y) * (req1.y - req2.y);
   return sqrt(cost);
 }

void calculateOfflineStrategy(int x, int y){
  acceptingRequests = false;
  offlineStrategyCost = LIM;

  //original server postitions
  serviceRequests[0].x = YELLOW_SERVER_POS_X;
  serviceRequests[0].y = YELLOW_SERVER_POS_Y;
  serviceRequests[1].x = RED_SERVER_POS_X;
  serviceRequests[1].y = RED_SERVER_POS_Y;
  serviceRequests[2].x = BLUE_SERVER_POS_X;
  serviceRequests[2].y = BLUE_SERVER_POS_Y;

  yellowServer.x = YELLOW_SERVER_POS_X;
  yellowServer.y = YELLOW_SERVER_POS_Y;
  redServer.x = RED_SERVER_POS_X;
  redServer.y = RED_SERVER_POS_Y;
  blueServer.x = BLUE_SERVER_POS_X;
  blueServer.y = BLUE_SERVER_POS_Y;

  int n,o,p,q;
  //entry cost[t][i][j][k] is the cost of the cheapest sequence of moves  
  int i,j,k,l;
  int reqI, reqJ, reqK;
  int preReqI, preReqJ, preReqK;
  
  //n*n*n*n matrix
  for (n=0; n<requestCount; n++){
    for (o=0; o<requestCount; o++){
      for (p=0; p<requestCount; p++){
        for (q =0; q<requestCount; q++){
            costMatrixArr[n][o][p][q] = LIM;
        }
      }
    }
  }

  
  costMatrixArr[2][0][1][2] = 0;
  //yellow. red. blue server

  for(n = 3; n < requestCount; n++) {
    for(i = 0; i < n+1; i++) {
      for(j = 1; j < n+1; j++) {
        for(k = 2; k < n+1; k++) {

          if(n != i && n != j && n != k) 
            costMatrixArr[n][i][j][k] = LIM; 
          else {
            for(l = 0; l < n; l++) {
              reqI = costMatrixArr[n-1][l][j][k] + calculateCost(serviceRequests[i], serviceRequests[l]);
              if (costMatrixArr[n][i][j][k] > reqI){
                costMatrixArr[n][i][j][k] = reqI;
                preReqI = l;
              }
             } 
            reqI = costMatrixArr[n][i][j][k];
     
            for(l = 0; l < n; l++) {
              reqJ = costMatrixArr[n-1][i][l][k] + calculateCost(serviceRequests[j], serviceRequests[l]);
              if (costMatrixArr[n][i][j][k] > reqJ) {
                costMatrixArr[n][i][j][k] = reqJ;
                preReqJ = l;
              }
            }
              reqJ = costMatrixArr[n][i][j][k];

            for(l = 0; l < n; l++) {
              reqK = costMatrixArr[n-1][i][j][l] + calculateCost(serviceRequests[k], serviceRequests[l]);
              if(costMatrixArr[n][i][j][k] > reqK) {
                costMatrixArr[n][i][j][k] = reqK;
                preReqK = l;
              }
            }
            reqK = costMatrixArr[n][i][j][k];

            costMatrixArr[n][i][j][k] = cheapestCost(reqI, reqJ, reqK);
            if(costMatrixArr[n][i][j][k] == reqI) 
              maxMatrixArr[n][i][j][k] = preReqI;
            else if( costMatrixArr[n][i][j][k] == reqJ ) 
              maxMatrixArr[n][i][j][k] = preReqJ;
            else if(costMatrixArr[n][i][j][k] == reqK) 
              maxMatrixArr[n][i][j][k] = preReqK;
          }
        }
      }
    }
  }

  for(i = 0; i < requestCount; i++) {
    for(j = 1; j < requestCount; j++) {
      for(k = 2; k < requestCount; k++) {
        if(costMatrixArr[requestCount-1][i][j][k] < offlineStrategyCost) {
          offlineStrategyCost = costMatrixArr[requestCount-1][i][j][k];
          reqI = i;
          reqJ = j;
          reqK = k;
        }
      }
    }
  }

  for (n=requestCount-1;n>=3;n--){
    if(reqI == n) 
    {
      XDrawLine(display_ptr, win, gc_yellow_thick_line, 
              serviceRequests[n].x, serviceRequests[n].y, 
            serviceRequests[maxMatrixArr[n][reqI][reqJ][reqK]].x, serviceRequests[maxMatrixArr[n][reqI][reqJ][reqK]].y);
      reqI = maxMatrixArr[n][reqI][reqJ][reqK];
    }
    else if(reqJ == n) 
    {
      XDrawLine(display_ptr, win, gc_red_thick_line, 
              serviceRequests[n].x, serviceRequests[n].y, 
            serviceRequests[maxMatrixArr[n][reqI][reqJ][reqK]].x, serviceRequests[maxMatrixArr[n][reqI][reqJ][reqK]].y);
      reqJ = maxMatrixArr[n][reqI][reqJ][reqK];
    }
    else if(reqK == n) 
    {
      XDrawLine(display_ptr, win, gc_blue_thick_line, 
              serviceRequests[n].x, serviceRequests[n].y, 
            serviceRequests[maxMatrixArr[n][reqI][reqJ][reqK]].x, serviceRequests[maxMatrixArr[n][reqI][reqJ][reqK]].y);
      reqK = maxMatrixArr[n][reqI][reqJ][reqK];
    }

  }

  printReport();
}

void printReport(){
  //offlineStrategyCost = 1; //temp
  printf("Total length of online strategy is: %d\n", onlineStrategyCost);
  printf("Total length of offline strategy is: %d\n", offlineStrategyCost);
  printf("Competitiveness ratio achieved\n(Online : Offline) is %f : 1\n", 
    (float)onlineStrategyCost / (float)offlineStrategyCost);
}

int cheapestCost(int reqI, int reqJ, int reqK){
  int cheapest;

  cheapest = (reqI <= reqJ) ? reqI : reqJ;
  cheapest = (reqK <= cheapest) ? reqK : cheapest;

  return cheapest;
}

void resetRequestQueues(){
    acceptingRequests = true;

    requestCount = 0;
    proximityServerYellow = 0;
    proximityServerBlue = 0;
    proximityServerRed = 0;
    onlineStrategyCost = 0;
    offlineStrategyCost = 0;

    //reset window
    XFillRectangle(display_ptr, win, gc_white, 0, 0, boundingX, boundingY);
    positionServers();      
}